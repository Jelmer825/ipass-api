# README #

### What is this repository for? ###

* This repository is for the api of the recommended data.
* These endpoints are called in the frontend.

### Setup ###

* Make a database according to the ERD. *
* Create a .env file 
* Install modules 

#### ENV example ####

* DB_LIVE_HOST=
* DB_LIVE_DATABASE=
* DB_LIVE_USERNAME=
* DB_LIVE_PASSWORD=
* 
* DB_ANALYSE_HOST=
* DB_ANALYSE_DATABASE=
* DB_ANALYSE_USERNAME=
* DB_ANALYSE_PASSWORD=

### Install modules

* pip install Flask         2.0.1
* pip install Flask-Cors    3.0.10
* pip install mysqlclient   2.0.3
* pip install pdoc3         0.9.2
* pip install python-dotenv 0.18.0


### Run tests
* python Tests.py

### Run program
* python api.py
