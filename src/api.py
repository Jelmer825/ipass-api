import flask
from flask import request, jsonify
from src.Database import Database
from flask_cors import CORS, cross_origin

app = flask.Flask(__name__)

# To allow cross origin
CORS(app)

app.config["DEBUG"] = True

@app.route('/recommendations', methods=['GET'])
def get_recommendations():
    """
    Get recommendations from database
    :return json response:
    """
    db = Database()

    if 'id' not in request.args:
        return None

    id = request.args['id']
    query = f"""SELECT * FROM recommendations WHERE company_one_id = {id} ORDER BY weight DESC LIMIT 100"""

    recommendations = db.get_query(query, True)
    return jsonify(recommendations)

@app.route('/company', methods=['GET'])
def get_company():
    """
    Get company from database
    :return json response:
    """
    db = Database()

    if 'id' not in request.args:
        return None

    id = request.args['id']
    query = f"""SELECT DISTINCT * FROM companies WHERE company_id = {id}"""

    company = db.get_query(query, True)
    return jsonify(company)

@app.route('/companies', methods=['GET'])
def get_companies():
    """
    Get companies from database
    :return json response:
    """
    db = Database()
    query = f"""SELECT * FROM companies"""

    company = db.get_query(query, True)
    return jsonify(company)
app.run()